﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestarQuitScript : MonoBehaviour
{
    public Text rounds;
    // Start is called before the first frame update

    private void Update()
    {
        rounds.text = "No. of Waves: " + WaveSpawner.rounds.ToString();
    }
    public void Restart()
    {
        
        SceneManager.LoadScene(1);
        gameObject.SetActive(false);
       
    }

    public void Quit()
    {
        Application.Quit();
    }
}
