﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MenuController : MonoBehaviour
{
    public GameObject crosshairs;
    private Vector3 target;

     bool p1, p2;

    public AudioSource source;

    public Animator p1Animator;
    public Animator p2Animator;

    public AudioClip clip;
    // Start is called before the first frame update
    void Start()
    {
        p1 = false;
        p2 = false;
        Cursor.visible = false;
        p1Animator.SetBool("Move", true);
        p2Animator.SetBool("Move", true);
    }

    // Update is called once per frame
    void Update()
    {
        target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        crosshairs.transform.position = new Vector2(target.x, target.y);



        if(Input.GetKey(KeyCode.Space))
        {
            p1 = true;
            p1Animator.SetBool("Move", false);
        }

        if(Input.GetMouseButtonDown(0))
        {
            source.PlayOneShot(clip);
        }

        if(p1 && p2)
        {
            SceneManager.LoadScene(1);
        }
        
    }

    public void Player2()
    {
        p2 = true;
        p2Animator.SetBool("Move", false);
    }

    
}
