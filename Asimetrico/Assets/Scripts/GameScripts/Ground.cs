﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ground : MonoBehaviour
{
    public int glife;

    public Sprite[] sprites;

    SpriteRenderer sp;
    Collider2D col;
    // Start is called before the first frame update
    void Start()
    {
        glife = 10;
        sp = GetComponent<SpriteRenderer>();
        col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if( glife>0 && glife < 3)
        {
            sp.sprite = sprites[2];
            col.isTrigger = true;
        }
        else if(glife >= 3 && glife<6)
        {
            sp.sprite = sprites[1];
            col.isTrigger = true;
        }
        else if(glife <= 0)
        {
            glife = 0;
            sp.sprite = null;
            col.isTrigger = false;
        }
        else if( glife >= 10)
        {
            glife = 10;
        }
        else
        {
            
            sp.sprite = sprites[0];
            col.isTrigger = true;
        }

       
    }
}
