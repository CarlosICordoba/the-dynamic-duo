﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Movimiento : MonoBehaviour
{
   public Image image;


    public Vector2 min;
    public Vector2 max;

    public GameObject restart;
    int directionx;
    int directiony;

    public Text groundText;
    public Text bombsText;
    public static int direction;

    public float speed;
    Vector2 movimiento;
    public static int bulletsRecovered;
    public static int fragmentsRecovered;
    public static int groundRecovered;

    public GameObject bomb;
    public  List<GameObject> bombs;

    public SpriteRenderer bombImage;


    float xAxis;
    float yAxis;
    Vector2 randonPosition;

    public int life;

    public int size;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        restart.SetActive(false);
        bulletsRecovered = 0;
        image.fillAmount = life / 10;
    }

    // Update is called once per frame
    void Update()
    {

        image.fillAmount = (float)life / 10;

        if (bombs.Count < 1 && bombs != null)
            bombImage.color = new Color(255, 255, 255, 0.5f);
        else
            bombImage.color = new Color(255, 255, 255, 1);

        xAxis = Random.Range(min.x, max.x);

        yAxis = Random.Range(min.y, max.y);

        randonPosition = new Vector2(xAxis, yAxis);





        Debug.LogError(life);
        groundText.text = "(" + groundRecovered.ToString() + ")";

        bombsText.text = "(" + bombs.Count.ToString() + ")";
     
        size = bombs.Count;
        if (Input.GetKey(KeyCode.D))
        {

            movimiento = Vector2.right;
            direction = 0;

        }

        else if (Input.GetKey(KeyCode.A))
        {

            movimiento = Vector2.left;
            direction = 180;
        }

        else if (Input.GetKey(KeyCode.W))
        {

            movimiento = Vector2.up;

            direction = 90;
        }

        else if (Input.GetKey(KeyCode.S))
        {

            movimiento = Vector2.down;
            direction = -90;
        }
        else
        {

            movimiento = Vector2.zero;

        }

        if (Input.GetKey(KeyCode.B) && bombs.Count > 0)
        {
            DropBombs();
        }
      
        

        if(life <=  0)
        {
                
            Time.timeScale = 0;
            restart.SetActive(true);
            //Destroy(gameObject);
        }

        
        transform.Translate(movimiento * speed * Time.deltaTime);
        
        if(fragmentsRecovered == 3)
        {
            bombs.Add(bomb);
            fragmentsRecovered = 0;
        }
    }

    public void DropBombs()
    {
        
        for (int i = 0; i < bombs.Count; i++)
        {
            
            Instantiate(bombs[i], new Vector2(transform.position.x - 1 + i,transform.position.y), Quaternion.identity);
            


        }
        bombs.Clear();
    }


}
