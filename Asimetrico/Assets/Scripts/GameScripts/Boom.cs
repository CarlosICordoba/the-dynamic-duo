﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class Boom : MonoBehaviour
{
    SpriteRenderer sp;
    public GameObject bullet;
    public GameObject bomb;
    public float delay = 4.0f;
    public GameObject explosionEffect;
    float countdown;
    bool hasExploded = false;
    public float radius = 1.5f;

    

    AudioSource source;
    public AudioClip explosion;
    // Start is called before the first frame update
    void Start()
    {
        sp = GetComponent<SpriteRenderer>();
        source = GameObject.Find("AudioHolder").GetComponent<AudioSource>();
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(Time.timeScale);
        countdown -= Time.deltaTime;
        
      
    }

    public void Explode()
    {
        CameraShaker.Instance.ShakeOnce(2f, 2f, 0.1f, 1f);
        Instantiate(explosionEffect, transform.position, transform.rotation);
        source.PlayOneShot(explosion, 0.6f);
        Collider2D[] collider2s = Physics2D.OverlapCircleAll(transform.position, radius);

        foreach (Collider2D nearbyObject in collider2s)
        {


            if (nearbyObject.CompareTag("Enemy"))
            {
                nearbyObject.GetComponent<EnemyMove>().life -= 2;
                StartCoroutine(Flash(nearbyObject.gameObject, Color.white, Color.red));
            }

            if(nearbyObject.CompareTag("Player"))
            {
                nearbyObject.GetComponent<Movimiento>().life -= 2;
                
                StartCoroutine(Flash(nearbyObject.gameObject, Color.white, Color.black));
            }
            
            if (nearbyObject.CompareTag("Ground"))
            {
                StartCoroutine(Flash(nearbyObject.gameObject, Color.white, Color.red));
                nearbyObject.GetComponent<Ground>().glife -= 3;
            }

        }


        Destroy(gameObject);
    }

    IEnumerator Flash(GameObject gO, Color color, Color color2)
    {
        gO.GetComponentInChildren<SpriteRenderer>().color = color;

        yield return new WaitForSeconds(0.05f);

        gO.GetComponentInChildren<SpriteRenderer>().color = color2;
    }

}
