﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    
   
    public int probabilidad;
    bool hasBullet;
    public GameObject ground;
    public int bomProb;
    public GameObject bullet;
    public GameObject bomb;
    public int life = 3;
    private void Start()
    {
        probabilidad = Random.Range(1, 11);
        bomProb = Random.Range(1, 11);
    }

    private void Update()
    {
        if(life <= 0)
        {
            Player2Logic.score += 1;
                if (probabilidad <= 8)
                    Instantiate(bullet, transform.position, Quaternion.identity);

                if (probabilidad <= 3)
                    Instantiate(bomb,transform.position - new Vector3(0, 0.5f, 0), Quaternion.identity);

                if(probabilidad <= 2)
                    Instantiate(ground, transform.position - new Vector3(0, -0.5f, 0), Quaternion.identity);

            Destroy(gameObject);
            
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {

            StartCoroutine(Flash(collision.gameObject, Color.white, Color.black));
            
            collision.gameObject.GetComponent<Movimiento>().life -= 1;
        }
    }

    IEnumerator Flash(GameObject gO, Color color, Color color2)
    {
        gO.GetComponentInChildren<SpriteRenderer>().color = color;

        yield return new WaitForSeconds(0.05f);

        gO.GetComponentInChildren<SpriteRenderer>().color = color2;
    }
}
