﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BombDetonator : MonoBehaviour
{
    public GameObject[] bombas;
    
    // Start is called before the first frame update


    // Update is called once per frame
    private void Update()
    {
        if (bombas != null)
        {
            bombas = GameObject.FindGameObjectsWithTag("Bomb");
        }

    
    }


    public void Detonate()
    {
        Player2Logic.numOfBullets += 1;
        foreach (GameObject bomb in bombas)
        {
            bomb.GetComponent<Boom>().Explode();
        }
    }
}
