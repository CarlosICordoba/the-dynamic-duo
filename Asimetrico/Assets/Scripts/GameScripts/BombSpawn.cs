﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawn : MonoBehaviour
{

    public Vector2 min;
    public Vector2 max;

    public GameObject[] bombas;

    float xAxis;
    float yAxis;
    Vector2 randonPosition;

    // Start is called before the first frame update
    void Start()
    {
       
        for (int i = 0; i < bombas.Length; i++)
        {
            xAxis = Random.Range(min.x, max.x);

            yAxis = Random.Range(min.y, max.y);

            randonPosition = new Vector2(xAxis, yAxis);
            Instantiate(bombas[i], randonPosition, Quaternion.identity);

        }
    }

   
}
