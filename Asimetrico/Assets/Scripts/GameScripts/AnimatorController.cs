﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour
{

    public Animator barraDeVida;

    public static bool  damage = false;

    private void Update()
    {
        if(damage)
        {
            barraDeVida.SetBool("Damage", true);
        }
        else
        {
            barraDeVida.SetBool("Damage", false);
        }
    }
}
