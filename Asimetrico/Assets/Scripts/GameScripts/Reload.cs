﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reload : MonoBehaviour
{
    public Button reload;
    public Button drop;


    public Vector2 min;
    public Vector2 max;
    public Movimiento movimiento;

    float xAxis;
    float yAxis;
    Vector2 randonPosition;

    AudioSource source;

    public AudioClip reloadClip;

    private void Start()
    {
        source = GameObject.Find("AudioHolder").GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Movimiento.bulletsRecovered < 1)
            reload.interactable = false;
        else
            reload.interactable = true;


    }

    public void ReloadGun()
    {
        source.PlayOneShot(reloadClip, 0.6f);
        Player2Logic.numOfBullets += Movimiento.bulletsRecovered + 1;
        Movimiento.bulletsRecovered = 0;
    }



}
