﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
     GameObject Player;
    
    public static int rounds = 0 ;
    public enum SpawnState {SPAWNING,WAITING,COUNTING};

    [System.Serializable]
    public class Wave
    {
       
        public string name;
        public Transform enemy;
        public int count;
        public float rate;
    }
    public int numberOfWaves = 100;
    public Wave[] waves;
    private int nextWave = 0;

    public Transform[] spawnPoints;
    

    public float timeBetweenWaves = 5f;
    public float waveCountdown;

    private float searchCountdown = 1f;

    
    private SpawnState state = SpawnState.COUNTING;

    public Transform enemyGo;

    public Text textWave;
   
    private void Start()
    {
        waves = new Wave[numberOfWaves];
        Player = GameObject.Find("Main Camera");
        for (int i = 0; i < waves.Length; i++)
        {
            waves[i] = new Wave();
        }

        for(int i = 0;i < waves.Length;i++)
        {
            waves[i].name = "Wave" + i;
            waves[i].enemy = enemyGo;
            waves[i].count = i + 2;
            waves[i].rate = i + 0.5f;
        }
        waveCountdown = timeBetweenWaves;

       

    }
    private void Update()
    {

        if(state == SpawnState.WAITING)
        {
            textWave.gameObject.SetActive(false);

            if(!EnemyIsAlive())
            {
                
                WaveCompleted();
            }
            else
            {
                return;
            }
        }

        if(waveCountdown <= 0)
        {
            textWave.gameObject.SetActive(false);

            
            Player.GetComponent<Player2Logic>().canShoot = true;

            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }
        }
        else
        {
            textWave.gameObject.SetActive(true);
           
            waveCountdown -= Time.deltaTime;

            Player.GetComponent<Player2Logic>().canShoot = false;

            textWave.text = ((int)waveCountdown).ToString();
        }
    }
    IEnumerator SpawnWave(Wave _wave)
    {
        state = SpawnState.SPAWNING;

        for(int i =0; i < _wave.count;i++)
        {
            SpawnEnemy(_wave.enemy);
            yield return new WaitForSeconds(1f / _wave.rate);
        }

        state = SpawnState.WAITING;

        yield break;
    }

    bool EnemyIsAlive()
    {
        searchCountdown -= Time.deltaTime;

        if(searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
                return false;
        }
       
        return true;
    }
    void WaveCompleted()
    {
        rounds += 1;
        state = SpawnState.COUNTING;
        waveCountdown = timeBetweenWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
        }
        else
        {
            nextWave++;
        }

    }
    void SpawnEnemy(Transform _enemy)
    {
       
        Transform _sp = spawnPoints[Random.Range(0,spawnPoints.Length)];
        Instantiate(_enemy, _sp.position,_sp.rotation);
    }
}
