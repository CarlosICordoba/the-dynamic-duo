﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
using UnityEngine.UI;

public class Player2Logic : MonoBehaviour
{
    public GameObject restar;
    public GameObject crosshairs;
    public GameObject bullet;
    public GameObject bomb;
    public static int numOfBullets;
    public bool canShoot;


    public GameObject player;

    public static int score;
    public Text textScore;
    
    private Vector3 target;
    EnemyMove enemyMove;

    public Color flashColor;
  

    AudioSource source;
    public AudioClip gunShot;

    public Text bulletsText;
    public Text bullestToReload;
    // Use this for initialization
    void Start ()
    {
        score = 0;
        source = GameObject.Find("AudioHolder").GetComponent<AudioSource>();

        Cursor.visible = false;
        canShoot = true;
        numOfBullets = 40;

        player = GameObject.Find("Player");


        restar.SetActive(false);


    }
    
    // Update is called once per frame
    void Update ()
    {
        textScore.text = "Score: " + score.ToString();
        bulletsText.text = "(" + numOfBullets.ToString() + ")";
        bullestToReload.text = "(" + Movimiento.bulletsRecovered.ToString() + ")";


        if(player.GetComponent<Movimiento>().life <= 0)
        {
            canShoot = false;
        }


        if (numOfBullets == 0 && Movimiento.bulletsRecovered == 0 )
        {
            Time.timeScale = 0;
            canShoot = false;
            restar.SetActive(true);

            

            Debug.Log("GameOver");
        }
        
        RaycastHit2D hit = Physics2D.Raycast(crosshairs.transform.position,Vector3.back);

        if(hit.collider != null)
        {
            Debug.Log(hit.collider.name);
        }

        target = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        crosshairs.transform.position = new Vector2(target.x, target.y);

        if(canShoot)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (hit.collider.CompareTag("Button"))
                {
                    canShoot = false;
                }
                else
                {
                    canShoot = true;
                }

                
                
                
                if (hit.collider.CompareTag("Player"))
                {
                    
                    CameraShaker.Instance.ShakeOnce(.2f, 3f, 0.1f, 1f);
                    numOfBullets--;
                    hit.collider.GetComponent<Movimiento>().life -= 1;
                    StartCoroutine(Flash(hit.collider.gameObject,Color.white,Color.black));
                    source.PlayOneShot(gunShot, 1);
                }
                if (hit.collider.CompareTag("Enemy"))
                {
                    CameraShaker.Instance.ShakeOnce(.2f, 3f, 0.1f, 1f);
                    numOfBullets--;
                    StartCoroutine(Flash(hit.collider.gameObject,Color.white,Color.red));
                    
                    hit.collider.GetComponent<EnemyMove>().life -= 1;

                    source.PlayOneShot(gunShot, 1);

                }
                if(hit.collider.CompareTag("Ground"))
                {
                    CameraShaker.Instance.ShakeOnce(.2f, 3f, 0.1f, 1f);
                    numOfBullets--;
                    StartCoroutine(Flash(hit.collider.gameObject, Color.red, Color.white));
                    hit.collider.GetComponent<Ground>().glife -= 1;

                    source.PlayOneShot(gunShot, 1);
                }
                if(hit.collider.CompareTag("Bomb") )
                {
                    hit.collider.GetComponent<Boom>().Explode();
                    CameraShaker.Instance.ShakeOnce(.2f, 3f, 0.1f, 1f);
                    numOfBullets--;
                    
                    source.PlayOneShot(gunShot, 1);
                }

               
            }

            
        }

        if(numOfBullets <= 0)
        {
            numOfBullets = 0;
            canShoot = false;
        }
        if(numOfBullets > 0)
        {
            canShoot = true;
        }
       
    }
   IEnumerator Flash(GameObject gO,Color color,Color color2)
    {
        gO.GetComponentInChildren<SpriteRenderer>().color = color;

        yield return new WaitForSeconds(0.05f);

        gO.GetComponentInChildren<SpriteRenderer>().color = color2;
    }
}
