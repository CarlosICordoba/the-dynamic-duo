﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GroundRegenartor : MonoBehaviour
{
    public GameObject[] ground;

    public Button restoreFloor;
    // Start is called before the first frame update
    void Start()
    {
        ground = GameObject.FindGameObjectsWithTag("Ground");

       
    }
    private void Update()
    {
        
            if ( Movimiento.groundRecovered <= 0 )
            {
                restoreFloor.interactable = false;
            }
            else
            {
                restoreFloor.interactable = true;
            }
        
    }

    // Update is called once per frame
    public void RestoreFloor()
    {
        
        foreach (GameObject ground in ground)
        {
            if(ground.GetComponent<Ground>().glife >= 0 && ground.GetComponent<Ground>().glife <= 10)
            {
                ground.GetComponent<Ground>().glife += Movimiento.groundRecovered;
                
            }
        }
        Movimiento.groundRecovered = 0;
    }
}
