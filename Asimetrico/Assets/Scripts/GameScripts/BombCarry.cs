﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombCarry : MonoBehaviour
{
    public static  bool hasBomb;

    GameObject bomb;
    private void Start()
    {
        hasBomb = false;
    }

    private void Update()
    {
        if(bomb != null && hasBomb)
        {
            if(Input.GetKey(KeyCode.G))
            {
                bomb.transform.SetParent(null);
                hasBomb = false;
                bomb.GetComponent<BombHolder>().grabbed = true;
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Bomb") )
        {
            Debug.Log("bomb");
            if (Input.GetKey(KeyCode.F) && !hasBomb)
            {
                bomb = collision.gameObject;
                if (!hasBomb && !bomb.GetComponent<BombHolder>().grabbed)
                {
                    bomb.transform.SetParent(gameObject.transform);
                    hasBomb = true;

                    
                }
                else
                {
                    collision.gameObject.transform.SetParent(null);
                    hasBomb = false;
                }
                
            }
        }
        
    }

}
